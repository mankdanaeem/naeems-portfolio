#Define Username
$username= Read-Host -Prompt "Enter Username: "

#Sets path where to save CSV file
$path= "C:\" + "$username" + ".csv"


#Runs DS Query to get memberships and exports CSV
#dsquery user -samid $username | dsget user -memberof > $path
Get-ADprincipalGroupmembership  -Identity $username | Get-AdGroup -properties * | Select Name, Description, Notes | Export-Csv -Path $path -NoTypeInformation -Encoding UTF8 -Force 


#Imports CSV 
$csv = Get-Content -Path $path
$csv2 = Import-csv  -Path $path

#Finds and Replaces text
$csv2 | ForEach-Object {

    $name = $_.Name
    $desc = $_.Descriptionente

    $name -replace "CN=","" `
    -replace "`"","" `
    -replace "qAll.*","" `
    -replace "OU=.*","" `
    -replace "qO365.*",""`
    -replace "Disclaimer.*",""`
    -replace "Messagelab.*",""`
    -replace "#TYPE.*",""`
    -replace "Domain Users",""

    

} | Set-Clipboard



