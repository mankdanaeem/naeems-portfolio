﻿$TargetFile = "$env:C:\Users\%username%\AppData\Local\Microsoft\Teams\current\Teams.exe"
$ShortcutFile = "$env:Public\Desktop\Microsoft Teams.lnk"
$WScriptShell = New-Object -ComObject WScript.Shell
$Shortcut = $WScriptShell.CreateShortcut($ShortcutFile)
$Shortcut.TargetPath = $TargetFile
$Shortcut.Save()