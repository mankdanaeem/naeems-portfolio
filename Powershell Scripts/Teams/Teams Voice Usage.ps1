# Check if the required modules are installed and install them if necessary
if (-not (Get-Module -Name AzureAD)) {
    Install-Module -Name AzureAD -Scope CurrentUser
}
if (-not (Get-Module -Name MicrosoftTeams)) {
    Install-Module -Name MicrosoftTeams -Scope CurrentUser
}
if (-not (Get-Module -Name ImportExcel)) {
    Install-Module -Name ImportExcel -Scope CurrentUser
}

# Define credentials
$username = "svc_o365mgmttools@dkp.com"
$passwordfile = "\\dkp.com\core\Installers\O365MgmtTools\DoNotDelete\SecureString.txt"
$keyfile = "\\dkp.com\core\Installers\O365MgmtTools\DoNotDelete\AES.key"
$key = Get-Content $keyfile
$password = Get-Content $passwordfile | ConvertTo-SecureString -Key $key
$cred = New-Object System.Management.Automation.PSCredential($username, $password)



# Connect to Microsoft Teams
Connect-MsolService -Credential $cred
Connect-MicrosoftTeams -Credential $cred


# Retrieve list of users
$users = Get-CsOnlineUser

# Export data to Excel file and save file path
$filePath = "C:\TeamsUsers.xlsx"
$users | Where-Object {$_.LineURI -ne $null} |
        Select-Object DisplayName, LineURI, @{Name="Last4LineURI"; Expression={$_.LineURI.Substring($_.LineURI.Length-4)}}, DialPlan |
        Sort-Object "Last4LineURI" |
        #Export-Csv -Path $filePath -NoTypeInformation
        Export-Excel -Path $filePath  -AutoSize -AutoFilter






# Define email variables
#$MailRecipient = "nmankda@dkp.com"
#$MailSender = " Mailbox Notification <TeamsUsers@dkp.com>"
#$Subject = 'Latest Teams Phone Users'
#$SMTPServer = 'dkp-com.mail.protection.outlook.com'
#$SMTPServer1 = 'dkrelay.dkp.com'


# Create email message
#$Message = New-Object System.Net.Mail.MailMessage($MailSender, $MailRecipient, $Subject, "Attached is the latest list of Teams phone users.")

# Add attachment
#$Attachment = New-Object System.Net.Mail.Attachment($filePath)
#$Message.Attachments.Add($Attachment)

# Send email
#$SMTPClient = New-Object Net.Mail.SmtpClient($SMTPServer)
#$SMTPClient.Send($Message)
