﻿$Username= Read-Host "Enter Username for User whose calendar permissions you'd like to change"

$User2= Read-Host "Enter Username of User who you would like to give permissions to"

$Permissions = Read-Host "What Permissions would you like to give [Owner, Publishing Editor, Reviewer, etc...]"

Add-MailboxFolderPermission -Identity $username@dkp.com:\Calendar -User $user2@dkp.com -AccessRights $Permissions 