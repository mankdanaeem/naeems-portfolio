$user = 
$passwordfile = 
$KeyFile = 
$key = Get-Content $KeyFile
$creds = New-Object -TypeName System.Management.Automation.PSCredential ` -ArgumentList $User, (Get-Content $PasswordFile | ConvertTo-SecureString -Key $key)













#Email Variables
$MailSender = " Mailbox Notification <MailboxWarning@example.com>"
$Subject = 'IMPORTANT: Your mailbox is reaching capacity'
$SMTPServer = 
$SMTPServer1 = 




#EmailBody
$EmailBody1 = "Your Outlook mailbox is reaching capacity. Once your mailbox size is at 100% capacity (over 100GB), Microsoft will automatically block your ability to send and receive until your mailbox is under the limit. 

To avoid disruption, it is strongly recommended you cleanup your mailbox. Please see the attached guide.

If you need assistance, please contact IT Service Desk: ."


 

#Capacity Variable
$Upper = 90

#Output Variable
$export = "C:\test.csv"

#Connect to EXO
try{
                     Connect-ExchangeOnline -Credential $creds
              }
              catch{
                     [System.Windows.MessageBox]::Show("Error connecting to Exchange Online.")  
              }




#Get User Mailboxes
$MailboxUsers = Get-Mailbox -ResultSize Unlimited


#Loop throught User Mailboxes and get usage 
ForEach($A in $MailboxUsers){

    
    $MailboxUsage = $A | Get-MailboxStatistics | Select DisplayName, @{name="TotalItemSizeGB"; expression={[math]::Round( ` ($_.TotalItemSize.ToString().Split("(")[1].Split(" ")[0].Replace(",","")/1024MB),2)}}, ItemCount 
     
   
        If($MailboxUsage.TotalItemSizeGB -gt $Upper) {

        $MailboxUsage | Export-Csv -Path $export -Append

         Echo $MailboxUsage.DisplayName "your mailbox is" $MailboxUsage.TotalItemSizeGB "GB" "Email is being sent to" $A.UserPrincipalName
            


         
         
         #Emails user with notification

         Send-MailMessage -To $A.UserPrincipalName -From $MailSender -SmtpServer $SMTPServer1 -Subject $Subject  -Body ($EmailBody1 | Out-String)  -Priority High
        }

    
        else {echo $MailboxUsage.DisplayName "Mailbox is below 90%"}


    }

#Email SD List of users mailboxes above 90GB

Send-MailMessage -To servicedesk@example.com -From $MailSender -SmtpServer $SMTPServer1 -Subject "90% and above mailboxes"  -Body "See attached"  -Attachments $export


    Write-Host "Export complete"
    Start-Sleep -Seconds 20

   Remove-Item $export
   
            
  



