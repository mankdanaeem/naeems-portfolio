Add-Type -AssemblyName System.Windows.Forms

$form = New-Object System.Windows.Forms.Form
$form.Text = "System Check"
$form.Width = 400
$form.Height = 400
$form.StartPosition = "CenterScreen"

$label = New-Object System.Windows.Forms.Label
$label.Location = New-Object System.Drawing.Point(10, 20)
$label.Size = New-Object System.Drawing.Size(280, 20)
$label.Text = "Enter the machine name to test:"
$form.Controls.Add($label)

$textbox = New-Object System.Windows.Forms.TextBox
$textbox.Location = New-Object System.Drawing.Point(10, 40)
$textbox.Size = New-Object System.Drawing.Size(200, 20)
$form.Controls.Add($textbox)

$button = New-Object System.Windows.Forms.Button
$button.Location = New-Object System.Drawing.Point(10, 70)
$button.Size = New-Object System.Drawing.Size(80, 23)
$button.Text = "Test"
$button.Add_Click({
    $pingable = Test-Connection $textbox.Text -Count 1 -Quiet

    $porticaService = Get-Service -ComputerName $textbox.Text -Name "PorticaService" -ErrorAction SilentlyContinue
    $porticaServiceStatus = $(if ($porticaService) {$porticaService.Status} else {"Service not found"})

    $brokerAgent = Get-Service -ComputerName $textbox.Text -Name "BrokerAgent" -ErrorAction SilentlyContinue
    $brokerAgentStatus = $(if ($brokerAgent) {$brokerAgent.Status} else {"Service not found"})

    $os = Get-WmiObject -ComputerName $textbox.Text -Class Win32_OperatingSystem
    $uptime = (Get-Date) - $os.ConvertToDateTime($os.LastBootUpTime)

    $LoggedInUsers = Get-WmiObject -Class Win32_ComputerSystem -ComputerName $textbox.Text | Select-Object -ExpandProperty UserName

    $pingResult = "$($textbox.Text) is " + $(if ($pingable) {"pingable"} else {"not pingable"})
    $serviceResult = "PorticaService status: $porticaServiceStatus`nBrokerAgent status: $brokerAgentStatus"
    $uptimeResult = "System uptime: $($uptime.Days) days, $($uptime.Hours) hours, $($uptime.Minutes) minutes"
    $LoggedOnResult = "Current logged in User $LoggedInUsers " 

    $resultLabel.Text = $pingResult + "`n`n" + $serviceResult + "`n`n" + $uptimeResult + "`n`n" + $LoggedOnResult 
})
$form.Controls.Add($button)


$buttonRestart = New-Object System.Windows.Forms.Button
$buttonRestart.Location = New-Object System.Drawing.Size(10,300)
$buttonRestart.Size = New-Object System.Drawing.Size(100,30)
$buttonRestart.Text = "Restart Services"
$buttonRestart.Add_Click({
    $service1 = Get-Service -Name "PorticaService" -ComputerName $textbox.Text
    $service2 = Get-Service -Name "BrokerAgent" -ComputerName $textbox.Text
    Restart-service -force -InputObject $service1,$service2
    [System.Windows.Forms.MessageBox]::Show("Services restarted on $($textbox.Text)")
})
$form.Controls.Add($buttonRestart)

$buttonReboot = New-Object System.Windows.Forms.Button
$buttonReboot.Location = New-Object System.Drawing.Size(130,300)
$buttonReboot.Size = New-Object System.Drawing.Size(100,30)
$buttonReboot.Text = "Reboot Machine"
$buttonReboot.Add_Click({
    $confirmation = [System.Windows.Forms.MessageBox]::Show("Are you sure you want to reboot $($textbox.Text)?", "Confirmation", [System.Windows.Forms.MessageBoxButtons]::YesNo)
    if ($confirmation -eq [System.Windows.Forms.DialogResult]::Yes) {
        Restart-Computer -ComputerName $textbox.Text -Force
    }
})
$form.Controls.Add($buttonReboot)




$resultLabel = New-Object System.Windows.Forms.Label
$resultLabel.Location = New-Object System.Drawing.Point(10, 100)
$resultLabel.Size = New-Object System.Drawing.Size(380, 120)
$resultLabel.Text = ""
$resultLabel.Font = New-Object System.Drawing.Font("Consolas", 9)
$resultLabel.TextAlign = [System.Drawing.ContentAlignment]::TopLeft
$form.Controls.Add($resultLabel)





$form.ShowDialog() | Out-Null
