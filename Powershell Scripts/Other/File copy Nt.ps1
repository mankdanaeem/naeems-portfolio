# Prompt the user for the remote machine's name or IP address
$remoteMachine = Read-Host -Prompt "Enter the name or IP address of the remote machine"

# Create a new PowerShell drive mapping to the remote machine's C drive
New-PSDrive -Name "A" -PSProvider FileSystem -Root "\\$remoteMachine\C$"

# Get a list of all the folders in the remote machine's C drive
$folders = Get-ChildItem -Path "A:\" -Directory

# Display the list of folders in an Out-GridView window
$selectedFolder = $folders | Out-GridView -Title "Select a folder" -OutputMode Single

# Set the source path to the selected folder on the remote machine
$source = $selectedFolder.FullName
$destination = "\\dkp.com\DFS\Public\Technology\POMO_Logs\$remoteMachine"

# Check if the destination folder exists
if (!(Test-Path -Path $destination)) {
  # Create the destination folder if it doesn't exist
  New-Item -ItemType Directory -Path $destination | Out-Null
}

# Copy the folder from the remote machine to the destination
Copy-Item -Path $source -Destination $destination -Recurse

# Confirm the copy was successful
if (Test-Path -Path "$destination\$(Get-ChildItem -Path $source | Select-Object -ExpandProperty Name)") {
  Write-Output "Folder copied successfully!"
} else {
  Write-Output "Error: Folder not found at destination."
}

# Remove the PowerShell drive mapping
Remove-PSDrive -Name "A"
