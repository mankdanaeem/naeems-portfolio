Get-PnpDevice -Class Media | Where-Object {$_.status -EQ "Ok"} | Disable-PnpDevice -Confirm:$false

Get-PnpDevice -Class AudioEndpoint | Where-Object {$_.status -NE "Error"} | Enable-PnpDevice -Confirm:$false