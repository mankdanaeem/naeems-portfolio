# Load the Windows Forms assembly
Add-Type -AssemblyName System.Windows.Forms

# Create a new form
$form = New-Object System.Windows.Forms.Form
$form.Text = "Install Printer"
$form.FormBorderStyle = [System.Windows.Forms.FormBorderStyle]::FixedDialog
$form.StartPosition = [System.Windows.Forms.FormStartPosition]::CenterScreen
$form.Size = New-Object System.Drawing.Size(400, 150)

# Create a label to display instructions
$label1 = New-Object System.Windows.Forms.Label
$label1.Location = New-Object System.Drawing.Point(10, 10)
$label1.Size = New-Object System.Drawing.Size(380, 20)
$label1.Text = "Select a printer to install from the list below:"
$form.Controls.Add($label1)

# Create a dropdown list to display printers
$printerList = Get-Printer -ComputerName "ln3print02" | Sort-Object Name  | select -expandproperty Name
$dropdown = New-Object System.Windows.Forms.ComboBox
$dropdown.Location = New-Object System.Drawing.Point(10, 40)
$dropdown.Size = New-Object System.Drawing.Size(380, 20)
$dropdown.DropDownStyle = [System.Windows.Forms.ComboBoxStyle]::DropDownList
$dropdown.DataSource = $printerList
$form.Controls.Add($dropdown)

# Create a button to install the selected printer
$button = New-Object System.Windows.Forms.Button
$button.Location = New-Object System.Drawing.Point(150, 80)
$button.Size = New-Object System.Drawing.Size(100, 30)
$button.Text = "Install Printer"
$button.Add_Click({
    $printerName = $dropdown.SelectedItem.ToString()
    Add-Printer -ConnectionName "\\ln3print02\$printerName"
    [System.Windows.Forms.MessageBox]::Show("Printer '$printerName' has been installed.", "Success")
})
$form.Controls.Add($button)

# Show the form and wait for the user to close it
$form.ShowDialog() | Out-Null
