# Set the source and destination paths
$src_path = "C:\logs"
$machine_name = $env:COMPUTERNAME
$dst_path = "C:\users\nmankda\test\$machine_name"

# Create the destination folder if it does not already exist
if (-not (Test-Path $dst_path)) {
    New-Item -ItemType Directory -Path $dst_path | Out-Null
}

# Copy the folder from the source to the destination
Copy-Item $src_path -Destination $dst_path -Recurse -Force
