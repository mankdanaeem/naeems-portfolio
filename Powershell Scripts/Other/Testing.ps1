#Skip Cert Check
$Cert = add-type @"
using System.Net;
using System.Security.Cryptography.X509Certificates;
public class TrustAllCertsPolicy : ICertificatePolicy {
    public bool CheckValidationResult(
        ServicePoint srvPoint, X509Certificate certificate,
        WebRequest request, int certificateProblem) {
        return true;
    }
}
"@
[System.Net.ServicePointManager]::CertificatePolicy = New-Object TrustAllCertsPolicy
$Cert

##SCRIPT 


# Get the list of IPs 
$ips = Get-Content "\\dkp.com\CORE\Scripts\VC\London VC IPs.txt"

# Set the credentials for the XAPI request
$username = "admin"
$password = "inl!w#nghu"
$credential = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $username, ($password | ConvertTo-SecureString -AsPlainText -Force)

# Set the headers for the XAPI request
$headers = @{
    Authorization = 'Basic ' + [Convert]::ToBase64String([Text.Encoding]::ASCII.GetBytes("$($credential.UserName):$($credential.GetNetworkCredential().Password)"))
    ContentType = 'text/xml'
}

# Loop through the array of IP addresses
foreach ($ipAddress in $ipAddresses) {
    # Construct the URL for the XAPI request
    $url = "https://$ipAddress/status.xml"

    # Send the XAPI request and retrieve the response
    $response = Invoke-WebRequest -Method Get -Uri $url -Headers $headers

    # Select the elements you want from the XML repsonse
    $xml = [xml]$response
    $software = $xml.Status.SystemUnit.Software.Version.InnerText
    $name = $xml.Status.SystemUnit.ContactInfo.InnerText
    


    # Display the extracted values for the current IP address
    "IP Address: $ipAddress"
     "Name: $name"
     "Version: $software."
    
}
