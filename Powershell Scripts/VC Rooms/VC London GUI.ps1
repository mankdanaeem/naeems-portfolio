Add-Type @"
    using System.Net;
    using System.Security.Cryptography.X509Certificates;
    public class TrustAllCertsPolicy : ICertificatePolicy {
        public bool CheckValidationResult(
            ServicePoint srvPoint, X509Certificate certificate,
            WebRequest request, int certificateProblem) {
            return true;
        }
    }
"@
[System.Net.ServicePointManager]::CertificatePolicy = New-Object TrustAllCertsPolicy

# Define the form
$Form = New-Object System.Windows.Forms.Form
$Form.Text = 'Dial Meeting'
$Form.Width = 300
$Form.Height = 150

# Define the label and text box for the meeting ID
$Label = New-Object System.Windows.Forms.Label
$Label.Location = New-Object System.Drawing.Point(10, 20)
$Label.Size = New-Object System.Drawing.Size(280, 20)
$Label.Text = 'Enter Meeting ID:'
$Form.Controls.Add($Label)

$TextBox = New-Object System.Windows.Forms.TextBox
$TextBox.Location = New-Object System.Drawing.Point(10, 40)
$TextBox.Size = New-Object System.Drawing.Size(280, 20)
$Form.Controls.Add($TextBox)

# Define the button to dial the meeting
$Button = New-Object System.Windows.Forms.Button
$Button.Location = New-Object System.Drawing.Point(10, 70)
$Button.Size = New-Object System.Drawing.Size(280, 30)
$Button.Text = 'Dial Meeting'
$Button.Add_Click({
    # Update the $Meeting variable with the value from the text box
    $global:Meeting = $TextBox.Text

    # Get the list of IPs 
    #$ipAddresses = Get-Content "\\dkp.com\CORE\Scripts\VC\London VC IPs.txt"
    $ipAddresses = Get-Content "\\dkp.com\CORE\Scripts\VC\test.txt"


    # Set the credentials for the XAPI request
    $username = "admin"
    $password = "inl!w#nghu"
    $credential = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $username, ($password | ConvertTo-SecureString -AsPlainText -Force)

    # Set the headers for the XAPI request
    $headers = @{
        Authorization = 'Basic ' + [Convert]::ToBase64String([Text.Encoding]::ASCII.GetBytes("$($credential.UserName):$($credential.GetNetworkCredential().Password)"))
        ContentType = 'text/xml'
    }

    # Loop through the array of IP addresses
    foreach ($ipAddress in $ipAddresses) {
        # Construct the URL for the XAPI request
        $url = "https://$ipAddress/putxml"

        # Create the body for the XAPI request with the updated meeting ID
        $body = "<Command><Dial><Number>dkp*$global:Meeting@teams.bjn.vc</Number></Dial></Command>"
        
        # Send the XAPI request and retrieve the response
        $response = Invoke-WebRequest -Method POST -Uri $url -Headers $headers -Body $body -UseBasicParsing
    }

    # Show a message box to confirm that the meeting was dialed
    [System.Windows.Forms.MessageBox]::Show("Meeting $global:Meeting was dialed.")
})

$Form.Controls.Add($Button)

# Start the form
$global:Meeting = ""
$Form.ShowDialog() | Out-Null
