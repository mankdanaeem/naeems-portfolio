$UserCredential = Get-Credential
$Servername = Read-Host "Enter Server Name"
$Session = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri http://$Servername/PowerShell/ -Authentication Kerberos -Credential $UserCredential
Import-PSSession $Session -DisableNameChecking


$USERNAME= Read-Host "Enter Username you'd like to enable"


Enable-RemoteMailbox -Identity $USERNAME -RemoteRoutingAddress $USERNAME@.mail.onmicrosoft.com

